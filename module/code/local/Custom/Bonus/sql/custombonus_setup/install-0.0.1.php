<?php

$installer = $this;
$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$entityTypeId     = $setup->getEntityTypeId('customer');
$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

$setup->addAttribute('customer', 'bonus_invoice', array(
    'input'         => 'text', //or select or whatever you like
    'type'          => 'int', //or varchar or anything you want it
    'label'         => 'Bonus Payment',
    'visible'       => 1,
    'source'        => 'custombonus/bonus',
    'required'      => 0, //mandatory? then 1
    'user_defined' => 1,
));

$setup->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $attributeGroupId,
    'bonus_invoice',
    '100'
);

$oAttribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'bonus_invoice');
$oAttribute->setData('used_in_forms', array('adminhtml_customer'));
$oAttribute->save();

$setup->endSetup();